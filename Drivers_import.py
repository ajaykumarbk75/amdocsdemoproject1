# Databricks notebook source
from pyspark.sql.types import StructType, StructField, IntegerType, StringType, DateType


# COMMAND ----------

name_schema = StructType(fields=[StructField("forename", StringType(), True),
                                 StructField("surname", StringType(), True)
    ])

# COMMAND ----------

drivers_schema = StructType([StructField("driverId", IntegerType(), False),
                            StructField("driverRef", StringType(), True),
                            StructField("number", IntegerType(), True),
                            StructField("code", StringType(), True),
                            StructField("name", name_schema),
                            StructField("dob", DateType(), True),
                            StructField("nationality", StringType(), True),
                            StructField("url", StringType(), True)
                 
                ])

# COMMAND ----------

drivers_df = spark.read \
.schema(drivers_schema) \
.json("/FileStore/tables/drivers.json")

# COMMAND ----------

#rename columns & add new columns

from pyspark.sql.functions import col, concat, current_timestamp, lit


# COMMAND ----------

drivers_with_columns_df = drivers_df.withColumnRenamed("driverId", "driver_id") \
                                   .withColumnRenamed("driverRef", "driver_ref") \
                                   .withColumn("ingestion_date", current_timestamp()) \
                                   .withColumn("name", concat(col("name.forename"), lit(" "), col("name.surname"))) 


# COMMAND ----------

#drop unwanted columns

drivers_final_df = drivers_with_columns_df.drop(col("url"))


# COMMAND ----------

# write to output directory - in parquet format

drivers_final_df.write.mode("overwrite").parquet("/mnt/formula1dl/processed/drivers")

# COMMAND ----------

# MAGIC %fs
# MAGIC ls /mnt/formula1dl/processed/drivers

# COMMAND ----------

display(drivers_final_df.limit(10))

# COMMAND ----------


